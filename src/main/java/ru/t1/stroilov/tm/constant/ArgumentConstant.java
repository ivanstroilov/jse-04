package ru.t1.stroilov.tm.constant;

public final class ArgumentConstant {

    public static final String VERSION = "-v";

    public static final String INFO = "-i";

    public static final String HELP = "-h";

}
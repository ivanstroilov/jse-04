package ru.t1.stroilov.tm;

import ru.t1.stroilov.tm.constant.AppConstant;
import ru.t1.stroilov.tm.constant.ArgumentConstant;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        showWelcome();
        runApplication(args);
        System.out.println("Please enter command: ");
        runInput();
    }

    private static void runInput() {
        final Scanner scanner = new Scanner(System.in);

        while (!Thread.currentThread().isInterrupted()) {
            parseInputArgument(scanner.nextLine());
            System.out.println();
        }
    }

    private static void runApplication(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        switch (arg) {
            case ArgumentConstant.VERSION:
                showVersion();
                break;
            case ArgumentConstant.INFO:
                showDeveloperInfo();
                break;
            case ArgumentConstant.HELP:
                showHelp();
                break;
            default:
                showUnknownCommand(arg);
                break;
        }
        shutDownApplication();
    }

    private static void parseInputArgument(final String input) {
        if (input == null || input.isEmpty()) return;
        switch (input) {
            case AppConstant.VERSION:
                showVersion();
                break;
            case AppConstant.INFO:
                showDeveloperInfo();
                break;
            case AppConstant.HELP:
                showHelp();
                break;
            case AppConstant.EXIT:
                shutDownApplication();
                break;
            default:
                showUnknownCommand(input);
                break;
        }
    }

    private static void shutDownApplication() {
        System.exit(0);
    }

    private static void showWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf(
                "%s, %s - Display program version.\n" +
                        "%s, %s - Display developer info.\n" +
                        "%s, %s - Display list of terminal commands.\n" +
                        "%s - Terminate the application.",
                AppConstant.VERSION, ArgumentConstant.VERSION,
                AppConstant.INFO, ArgumentConstant.INFO,
                AppConstant.HELP, ArgumentConstant.HELP,
                AppConstant.EXIT
        );
        System.out.println();
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.4.0");
    }

    private static void showDeveloperInfo() {
        System.out.println("[DEVELOPER INFORMATION]");
        System.out.println("Stroilov Ivan");
        System.out.println("ivanstroilov@gmail.com");
    }

    private static void showUnknownCommand(final String arg) {
        System.out.printf("Command '%s' not supported.\n", arg);
        System.out.println();
    }

}